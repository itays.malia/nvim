local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  {
    "mfussenegger/nvim-dap",
    requires = {
      {
        "mxsdev/nvim-dap-vscode-js",
        requires = {
          "microsoft/vscode-js-debug",
          opt = true,
          run = "npm install --legacy-peer-deps && npm run compile",
        },
      },
      {
        "Joakker/lua-json5",
        run = "./install.sh",
      },
    },
    as = "dap",
    after = { "mason-dap" },
    config = function()
      local dap = safe_require("dap")
      if not dap then
        return
      end

      local json5 = safe_require("json5")
      if json5 then
        require("dap.ext.vscode").json_decode = json5.parse
      end

      local namespace = vim.api.nvim_create_namespace("dap-hlng")
      vim.api.nvim_set_hl(namespace, "DapBreakpoint", { fg = "#993939", bg = "#31353f" })
      vim.api.nvim_set_hl(namespace, "DapLogPoint", { fg = "#61afef", bg = "#31353f" })
      vim.api.nvim_set_hl(namespace, "DapStopped", { fg = "#98c379", bg = "#31353f" })
      vim.api.nvim_set_hl_ns(namespace)

      vim.fn.sign_define("DapBreakpoint", { text = "", texthl = "DapBreakpoint", numhl = "DapBreakpoint" })
      vim.fn.sign_define("DapBreakpointCondition", { text = "ﳁ", texthl = "DapBreakpoint", numhl = "DapBreakpoint" })
      vim.fn.sign_define("DapBreakpointRejected", { text = "", texthl = "DapBreakpoint", numhl = "DapBreakpoint" })
      vim.fn.sign_define("DapLogPoint", { text = "", texthl = "DapLogPoint", numhl = "DapLogPoint" })
      vim.fn.sign_define("DapStopped", { text = "", texthl = "DapStopped", numhl = "DapStopped" })

      vim.keymap.set("n", "<leader>b", dap.toggle_breakpoint, { desc = "Toggle Breakpoint" })
      vim.keymap.set("n", "<leader>B", function()
        dap.set_breakpoint(vim.fn.input("Breakpoint condition: "))
      end, { desc = "Set Breakpoint with condition" })

      local wk = safe_require("which-key", true)
      if wk then
        wk.register({ ["<leader>d"] = { name = "DAP" } })
      end

      vim.keymap.set("n", "<leader>db", dap.toggle_breakpoint, { desc = "Toggle Breakpoint" })
      vim.keymap.set("n", "<leader>dB", function()
        dap.set_breakpoint(vim.fn.input("Breakpoint condition: "))
      end, { desc = "Set Breakpoint with condition" })
      vim.keymap.set("n", "<leader>dp", function()
        dap.set_breakpoint(nil, nil, vim.fn.input("Log point message: "))
      end, { desc = "Set Logpoint" })
      vim.keymap.set("n", "<leader>dc", dap.continue, { desc = "Continue" })
      vim.keymap.set("n", "<leader>dt", function()
        dap.terminate()
        local dapui = safe_require("dapui")
        if dapui then
          dapui.close()
        end
      end, { desc = "Terminate" })
      vim.keymap.set("n", "<leader>dr", dap.repl.open, { desc = "Repl Open" })

      vim.keymap.set("n", "<F5>", dap.continue)
      vim.keymap.set("n", "<F10>", dap.step_over)
      vim.keymap.set("n", "<F11>", dap.step_into)
      vim.keymap.set("n", "<F12>", dap.step_out)

      vim.api.nvim_create_autocmd("FileType", {
        pattern = "dap-repl",
        callback = function()
          -- vim.keymap.set({ "l", "t" }, "<C-j>", "<C-c>", { silent = true, buffer = 0 })
          vim.keymap.set("n", "q", function()
            if dap.session() == nil then
              vim.api.nvim_buf_delete(0, { force = true })
            end
          end, { silent = true, buffer = 0 })
        end,
      })
      local dap_node = safe_require("dap-vscode-js")
      if dap_node then
        dap_node.setup({
          -- node_path = "node", -- Path of node executable. Defaults to $NODE_PATH, and then "node"
          -- debugger_path = "(runtimedir)/site/pack/packer/opt/vscode-js-debug", -- Path to vscode-js-debug installation.
          -- debugger_cmd = { "js-debug-adapter" }, -- Command to use to launch the debug server. Takes precedence over `node_path` and `debugger_path`.
          adapters = { "pwa-node", "pwa-chrome", "pwa-msedge", "node-terminal", "pwa-extensionHost" }, -- which adapters to register in nvim-dap
          -- log_file_path = "(stdpath cache)/dap_vscode_js.log" -- Path for file logging
          -- log_file_level = false -- Logging level for output to file. Set to false to disable file logging.
          -- log_console_level = vim.log.levels.ERROR -- Logging level for output to console. Set to false to disable console output.
        })

        for _, language in ipairs({ "typescript", "javascript" }) do
          dap.configurations[language] = {
            {
              type = "pwa-node",
              request = "launch",
              name = "Launch file",
              program = "${file}",
              cwd = "${workspaceFolder}",
            },
            {
              type = "pwa-node",
              request = "attach",
              name = "Attach",
              processId = require("dap.utils").pick_process,
              cwd = "${workspaceFolder}",
            },
            {
              type = "pwa-node",
              request = "launch",
              name = "Debug Jest Tests",
              -- trace = true, -- include debugger info
              runtimeExecutable = "node",
              runtimeArgs = {
                "./node_modules/jest/bin/jest.js",
                "--runInBand",
              },
              rootPath = "${workspaceFolder}",
              cwd = "${workspaceFolder}",
              console = "integratedTerminal",
              internalConsoleOptions = "neverOpen",
            },
          }
        end
      end

      local mason_dap = safe_require("mason-nvim-dap")
      if mason_dap then
        mason_dap.setup_handlers({
          function(source_name)
            require("mason-nvim-dap.automatic_setup")(source_name)
          end,
        })
      end
    end,
  },
  {
    "jay-babu/mason-nvim-dap.nvim",
    as = "mason-dap",
    after = { "mason" },
    config = function()
      local mason_dap = safe_require("mason-nvim-dap")
      if not mason_dap then
        return
      end
      mason_dap.setup({ automatic_setup = { exclude = { "node2" } } })
    end,
  },
  {
    "nvim-telescope/telescope-dap.nvim",
    after = { "dap", "treesitter", "telescope" },
    config = function()
      local telescope = safe_require("telescope")
      if not telescope then
        return
      end
      telescope.load_extension("dap")
    end,
  },
  {
    "rcarriga/nvim-dap-ui",
    after = { "dap" },
    config = function()
      local dapui = safe_require("dapui")
      if not dapui then
        return
      end
      dapui.setup({})

      local dap = safe_require("dap")
      if not dap then
        return
      end
      dap.listeners.after.event_initialized["dapui_config"] = dapui.open
      dap.listeners.before.event_terminated["dapui_config"] = dapui.close
      dap.listeners.before.event_exited["dapui_config"] = dapui.close

      vim.keymap.set({ "n", "v" }, "<M-k>", dapui.eval)
    end,
  },
})
