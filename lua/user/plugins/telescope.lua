local builtin = require("lazy-require").require_on_exported_call("telescope.builtin")

return {
  "nvim-telescope/telescope.nvim",
  dependencies = {
    { "nvim-lua/plenary.nvim" },
    { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
    { "nvim-telescope/telescope-frecency.nvim", dependencies = { "tami5/sqlite.lua" } },
    { "LinArcX/telescope-command-palette.nvim" },
    { "gbrlsnchs/telescope-lsp-handlers.nvim" },
    { "nvim-telescope/telescope-symbols.nvim" },
  },
  keys = {
    {
      "<C-p>",
      function()
        if vim.fn.isdirectory(".git") ~= 0 then
          builtin.git_files()
        else
          builtin.find_files()
        end
      end,
    },
    {
      "<leader>sF",
      function()
        builtin.find_files()
      end,
      desc = "Find All Files",
    },
    {
      "<leader>sf",
      function()
        if vim.fn.isdirectory(".git") ~= 0 then
          builtin.git_files()
        else
          builtin.find_files()
        end
      end,
      desc = "Find Files",
    },
    {
      "<leader>sc",
      function()
        require("telescope").extensions.command_palette.command_palette()
      end,
      desc = "Command Palette",
    },
    {
      "<leader>se",
      function()
        builtin.symbols({ sources = { "emoji" } })
      end,
      desc = "Emoji",
    },
    {
      "<leader>sE",
      function()
        builtin.symbols()
      end,
      desc = "Symbols",
    },
    {
      "<leader>sa",
      function()
        require("telescope").extensions.frecency.frecency()
      end,
      desc = "Frecency",
    },
    {
      "<leader>ss",
      function()
        builtin.live_grep()
      end,
      desc = "Live Grep",
    },
    {
      "<leader>sm",
      function()
        builtin.man_pages()
      end,
      desc = "Man Pages",
    },
    {
      "<leader>sh",
      function()
        builtin.help_tags()
      end,
      desc = "Help Tags",
    },
    {
      "<leader>sb",
      function()
        builtin.buffers()
      end,
      desc = "Buffers",
    },
    {
      "<leader>so",
      function()
        builtin.oldfiles()
      end,
      desc = "Old Files",
    },
    {
      "<leader>sr",
      function()
        builtin.lsp_references()
      end,
      desc = "LSP: References",
    },
    {
      "<leader>si",
      function()
        builtin.treesitter()
      end,
      desc = "TreeSitter: Items",
    },
    {
      "<leader>sgb",
      function()
        builtin.git_branches()
      end,
      desc = "Branches",
    },
    {
      "z=",
      function()
        builtin.spell_suggest()
      end,
      desc = "Telescope: Spelling",
    },
  },
  config = function()
    local telescope = require("telescope")

    telescope.load_extension("fzf")
    telescope.load_extension("frecency")
    telescope.load_extension("command_palette")
    telescope.load_extension("lsp_handlers")

    local actions = require("telescope.actions")

    telescope.setup({
      defaults = {
        mappings = {
          i = {
            ["<C-j>"] = actions.move_selection_next,
            ["<C-k>"] = actions.move_selection_previous,
          },
        },
      },
      pickers = {},
      extensions = {
        fzf = {
          fuzzy = true, -- false will only do exact matching
          override_generic_sorter = true, -- override the generic sorter
          override_file_sorter = true, -- override the file sorter
          case_mode = "smart_case", -- or "ignore_case" or "respect_case"
          -- the default case_mode is "smart_case"
        },
        frecency = {
          show_scores = true,
          show_unindexed = false,
          ignore_patterns = { "*.git/*", "*/tmp/*" },
          disable_devicons = false,
        },
        -- command_palette = {
        -- {"File",
        -- { "entire selection (C-a)", ':call feedkeys("GVgg")' },
        -- { "save current file (C-s)", ':w' },
        -- { "save all files (C-A-s)", ':wa' },
        -- { "quit (C-q)", ':qa' },
        -- { "file browser (C-i)", ":lua require'telescope'.extensions.file_browser.file_browser()", 1 },
        -- { "search word (A-w)", ":lua require('telescope.builtin').live_grep()", 1 },
        -- { "git files (A-f)", ":lua require('telescope.builtin').git_files()", 1 },
        -- { "files (C-f)",     ":lua require('telescope.builtin').find_files()", 1 },
        -- },
        -- {"Help",
        -- { "tips", ":help tips" },
        -- { "cheatsheet", ":help index" },
        -- { "tutorial", ":help tutor" },
        -- { "summary", ":help summary" },
        -- { "quick reference", ":help quickref" },
        -- { "search help(F1)", ":lua require('telescope.builtin').help_tags()", 1 },
        -- },
        -- {"Vim",
        -- { "reload vimrc", ":source $MYVIMRC" },
        -- { 'check health', ":checkhealth" },
        -- { "jumps (Alt-j)", ":lua require('telescope.builtin').jumplist()" },
        -- { "commands", ":lua require('telescope.builtin').commands()" },
        -- { "command history", ":lua require('telescope.builtin').command_history()" },
        -- { "registers (A-e)", ":lua require('telescope.builtin').registers()" },
        -- { "colorshceme", ":lua require('telescope.builtin').colorscheme()", 1 },
        -- { "vim options", ":lua require('telescope.builtin').vim_options()" },
        -- { "keymaps", ":lua require('telescope.builtin').keymaps()" },
        -- { "buffers", ":Telescope buffers" },
        -- { "search history (C-h)", ":lua require('telescope.builtin').search_history()" },
        -- { "paste mode", ':set paste!' },
        -- { 'cursor line', ':set cursorline!' },
        -- { 'cursor column', ':set cursorcolumn!' },
        -- { "spell checker", ':set spell!' },
        -- { "relative number", ':set relativenumber!' },
        -- { "search highlighting (F12)", ':set hlsearch!' },
        -- }
        -- }
      },
    })
  end,
}
