return {
  "aserowy/tmux.nvim",
  enabled = false,
  lazy = false,
  config = function()
    require("tmux").setup({
      copy_sync = {
        enable = true,
      },
      navigation = {
        enable_default_keybindings = true,
      },
      resize = {
        enable_default_keybindings = true,
      },
    })
  end,
}
