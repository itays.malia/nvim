return {
  {
    "lewis6991/gitsigns.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    lazy = false,
    config = function()
      local gitsigns = require("gitsigns")
      gitsigns.setup({
        on_attach = function(bufnr)
          local wk = safe_require("which-key", true)
          if wk then
            wk.register({ ["<leader>h"] = { name = "Git Signs" } }, { buffer = bufnr })
            wk.register({ ["<leader>h"] = { name = "Git Signs" } }, { mode = "v", buffer = bufnr })
          end
          vim.keymap.set({ "n", "v" }, "<leader>hs", gitsigns.stage_hunk, { desc = "Stage hunk", buffer = bufnr })
          vim.keymap.set("n", "<leader>hu", gitsigns.undo_stage_hunk, { desc = "Unstage hunk", buffer = bufnr })
          vim.keymap.set({ "n", "v" }, "<leader>hr", gitsigns.reset_hunk, { desc = "Reset hunk", buffer = bufnr })
          vim.keymap.set("n", "<leader>hR", gitsigns.reset_buffer, { desc = "Reset buffer", buffer = bufnr })
          vim.keymap.set("n", "<leader>hp", gitsigns.preview_hunk, { desc = "Preview hunk", buffer = bufnr })
          vim.keymap.set("n", "<leader>hb", function()
            gitsigns.blame_line({ full = true })
          end, { desc = "Blame line", buffer = bufnr })
          vim.keymap.set("n", "<leader>hS", gitsigns.stage_buffer, { desc = "Stage buffer", buffer = bufnr })
          vim.keymap.set(
            "n",
            "<leader>hU",
            gitsigns.reset_buffer_index,
            { desc = "Reset buffer index", buffer = bufnr }
          )
        end,
      })
    end,
  },
  {
    "TimUntersberger/neogit",
    dependencies = "nvim-lua/plenary.nvim",
    cmd = "Neogit",
    keys = { { "<leader>g", vim.cmd.Neogit, desc = "Neogit" } },
    config = function()
      require("neogit").setup({
        integrations = {
          diffview = true,
        },
      })
    end,
  },
}
