return {
  "kyazdani42/nvim-tree.lua",
  dependencies = { "kyazdani42/nvim-web-devicons" },
  keys = {
    {
      "<leader>-",
      function()
        vim.cmd.NvimTreeFindFile()
        vim.cmd.NvimTreeOpen()
        -- nvim_tree_api.tree.open()
      end,
      desc = "Open Explorer",
    },
  },
  config = function()
    require("nvim-tree").setup({
      filesystem_watchers = {
        ignore_dirs = { "node_modules", "target", ".git" },
      },
    })
    local nvim_tree_api = require("nvim-tree.api")

    vim.api.nvim_create_autocmd("FileType", {
      pattern = "NvimTree",
      callback = function()
        vim.keymap.set("n", "<leader>-", nvim_tree_api.tree.close, { desc = "Close Explorer", buffer = 0 })
      end,
    })
  end,
}
