return {
  "saecki/crates.nvim",
  tag = "v0.2.1",
  dependencies = { "nvim-lua/plenary.nvim" },
  ft = "toml",
  config = function()
    require("crates").setup()
  end,
}
