local toggleterm = require("lazy-require").require_on_exported_call("toggleterm")

return {
  "akinsho/toggleterm.nvim",
  keys = {
    {
      "<leader>tt",
      function()
        toggleterm.toggle(vim.v.count1)
      end,
      desc = "Toggle Term",
    },
    {
      "<leader>ta",
      function()
        toggleterm.toggle_all()
      end,
      desc = "Toggle All Term",
    },
  },
  config = function()
    toggleterm.setup({
      open_mapping = false,
    })
  end,
}
