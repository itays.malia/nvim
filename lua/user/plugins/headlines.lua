return {
  "lukas-reineke/headlines.nvim",
  ft = { "markdown", "rmd", "vimwiki", "orgmode" },
  config = function()
    require("headlines").setup()
  end,
}
