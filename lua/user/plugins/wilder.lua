return {
  "gelguy/wilder.nvim",
  enabled = false,
  dependencies = { { "roxma/vim-hug-neovim-rpc" }, { "kyazdani42/nvim-web-devicons" } },
  build = ":UpdateRemotePlugins",
  config = function()
    vim.fn["wilder#setup"]({ modes = { ":" } })
    vim.cmd([[
    call wilder#set_option('renderer', wilder#popupmenu_renderer(wilder#popupmenu_border_theme({
    \  'highlights': { 'border': 'Normal' },
    \  'border': 'rounded',
    \  'highlighter': wilder#basic_highlighter(),
    \  'left': [
    \    ' ', wilder#popupmenu_devicons(),
    \  ],
    \  'min_width': '100%',
    \  'reverse': 0,
    \ })))
    call wilder#set_option('pipeline', [
    \   wilder#branch(
    \     wilder#cmdline_pipeline({
    \       'language': 'python',
    \       'fuzzy': 1,
    \     }),
    \     wilder#python_search_pipeline({
    \       'pattern': wilder#python_fuzzy_pattern(),
    \       'sorter': wilder#python_difflib_sorter(),
    \       'engine': 're',
    \     }),
    \   ),
    \ ])
  ]] )
  end,
}
