return {
  "dstein64/vim-startuptime",
  cmd = "StartupTime",
  config = function()
    vim.g.startuptime_tries = 10

    vim.api.nvim_create_autocmd("FileType", {
      pattern = "startuptime",
      callback = function()
        vim.keymap.set("n", "q", vim.cmd.q, { buffer = 0 })
      end,
    })
  end,
}
