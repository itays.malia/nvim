return {
  "b3nj5m1n/kommentary",
  keys = {
    { "gcc", "<Plug>kommentary_line_default", desc = "Comment line" },
    { "gc", "<Plug>kommentary_line_default", desc = "Comment line" },
    { "gc", "<Plug>kommentary_visual_default<C-c>", mode = "v", desc = "Comment visual selection" },
  },
  config = function()
    local kommentary_config = require("kommentary.config")

    kommentary_config.configure_language("default", {
      prefer_single_line_comments = true,
    })
  end,
}
