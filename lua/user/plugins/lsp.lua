local format_on_save_group = vim.api.nvim_create_augroup("formatOnSave", { clear = true })

local function base_on_attach(_, bufnr)
  local format_async = function()
    vim.lsp.buf.format({ async = true })
  end

  vim.keymap.set("n", "<leader>f", format_async, { buffer = bufnr, desc = "LSP: Format" })

  local wk = safe_require("which-key", true)
  if wk then
    wk.register({ ["<leader>c"] = { name = "LSP" } }, { mode = "v" })
    wk.register({ ["<leader>c"] = { name = "LSP" } })
  end

  vim.keymap.set("n", "<leader>cf", format_async, { buffer = bufnr, desc = "Format" })
  vim.keymap.set("n", "<leader>cF", function()
    vim.lsp.buf.format()
  end, { buffer = bufnr, desc = "Format Sync" })

  vim.keymap.set("n", "<leader>ca", function()
    vim.lsp.buf.code_action()
  end, { buffer = bufnr, desc = "Code Action" })
  vim.keymap.set("v", "<leader>ca", function()
    vim.lsp.buf.range_code_action()
  end, { buffer = bufnr, desc = "Code Action" })

  vim.api.nvim_create_autocmd("BufWritePre", {
    group = format_on_save_group,
    buffer = bufnr,
    callback = function()
      vim.lsp.buf.format({ async = false })
    end,
  })
end

return {
  {
    "neovim/nvim-lspconfig",
    dependencies = { "onsails/diaglist.nvim", "b0o/SchemaStore.nvim", "folke/neodev.nvim", "folke/neoconf.nvim" },
    lazy = false,
    config = function()
      require("neoconf").setup()
      local lspconfig = require("lspconfig")

      local diaglist = require("diaglist")
      if not diaglist then
        return
      end

      vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, { desc = "Open float diagnostic" })
      vim.keymap.set("n", "[d", vim.diagnostic.goto_prev)
      vim.keymap.set("n", "[d", vim.diagnostic.goto_next)
      vim.keymap.set("n", "<leader>q", diaglist.open_buffer_diagnostics, { desc = "Open diagnostic list" })

      local wk = safe_require("which-key", true)
      if wk then
        wk.register({ ["<leader>c"] = { name = "LSP" } })
      end
      vim.keymap.set("n", "<leader>cR", vim.cmd.LspRestart, { desc = "Restart" })

      vim.api.nvim_create_autocmd("FileType", {
        pattern = "qf",
        callback = function()
          vim.keymap.set("n", "q", vim.cmd.q, { buffer = 0 })
        end,
      })

      local on_attach = function(client, bufnr)
        -- Enable completion triggered by <c-x><c-o>
        vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

        base_on_attach(client, bufnr)

        vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { buffer = bufnr })
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, { buffer = bufnr })
        vim.keymap.set("n", "K", vim.lsp.buf.hover, { buffer = bufnr })
        vim.keymap.set("n", "gi", vim.lsp.buf.implementation, { buffer = bufnr })
        vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, { buffer = bufnr })
        vim.keymap.set("n", "gr", vim.lsp.buf.references, { buffer = bufnr })

        if safe_require("inc_rename", true) then
          vim.keymap.set("n", "<leader>cr", function()
            return ":IncRename " .. vim.fn.expand("<cword>")
          end, { expr = true, buffer = bufnr, desc = "Rename" })
          -- vim.keymap.set("n", "<leader>cr", vim.cmd.IncRename, { expr = true, buffer = bufnr, desc = "Rename" })
          -- vim.keymap.set("n", "<leader>cr", vim.lsp.buf.rename, { buffer = bufnr, desc = "Rename" })
        else
          vim.keymap.set("n", "<leader>cr", vim.lsp.buf.rename, { buffer = bufnr, desc = "Rename" })
        end
        vim.keymap.set(
          "n",
          "<leader>cD",
          vim.lsp.buf.type_definition,
          { buffer = bufnr, desc = "Goto Type Definition" }
        )

        if wk then
          wk.register({ ["<leader>cw"] = { name = "Workspace" } }, { buffer = bufnr })
        end
        vim.keymap.set("n", "<leader>cwa", vim.lsp.buf.add_workspace_folder, { buffer = bufnr, desc = "Add" })
        vim.keymap.set("n", "<leader>cwr", vim.lsp.buf.remove_workspace_folder, { buffer = bufnr, desc = "Remove" })
        vim.keymap.set("n", "<leader>cwl", function()
          print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        end, { buffer = bufnr, desc = "List" })
      end

      local cmp_lsp = safe_require("cmp_nvim_lsp")
      if not cmp_lsp then
        return
      end

      local mason_lsp = safe_require("mason-lspconfig")
      if mason_lsp then
        mason_lsp.setup_handlers({
          function(server_name)
            lspconfig[server_name].setup({
              on_attach = on_attach,
              capabilities = cmp_lsp.default_capabilities(),
            })
          end,
          ["sumneko_lua"] = function(server_name)
            require("neodev").setup()
            lspconfig[server_name].setup({
              on_attach = on_attach,
              settings = {
                Lua = {
                  completion = {
                    callSnippet = "Replace",
                  },
                },
              },
              capabilities = cmp_lsp.default_capabilities(),
            })
          end,
          clangd = function(server_name)
            lspconfig[server_name].setup({
              on_attach = function(client, bufnr)
                client.server_capabilities.document_formatting = false
                client.server_capabilities.document_range_formatting = false
                on_attach(client, bufnr)
              end,
              capabilities = cmp_lsp.default_capabilities(),
            })
          end,
          tsserver = function(server_name)
            local typescript = safe_require("typescript")
            if typescript then
              typescript.setup({
                server = {
                  on_attach = function(client, bufnr)
                    client.server_capabilities.document_formatting = false
                    client.server_capabilities.document_range_formatting = false
                    vim.keymap.set("v", "<leader>ci", function()
                      typescript.actions.addMissingImports({ sync = true })
                      typescript.actions.removeUnused({ sync = true })
                      typescript.actions.organizeImports({ sync = true })
                    end, { buffer = bufnr, desc = "Fix Imports" })
                    on_attach(client, bufnr)
                  end,
                  capabilities = cmp_lsp.default_capabilities(),
                },
              })
            else
              lspconfig[server_name].setup({
                on_attach = function(client, bufnr)
                  client.server_capabilities.document_formatting = false
                  client.server_capabilities.document_range_formatting = false
                  on_attach(client, bufnr)
                end,
                capabilities = cmp_lsp.default_capabilities(),
              })
            end
          end,
          jsonls = function(server_name)
            lspconfig[server_name].setup({
              on_attach = on_attach,
              capabilities = cmp_lsp.default_capabilities(),
              settings = {
                json = {
                  schemas = vim.list_extend({
                    {
                      description = "Tarui configuration",
                      fileMatch = { "tauri.conf.json" },
                      name = "tauri.conf.json",
                      url = "https://raw.githubusercontent.com/tauri-apps/tauri/dev/tooling/cli/schema.json",
                    },
                  }, require("schemastore").json.schemas({})),

                  validate = { enable = true },
                },
              },
            })
          end,
          tailwindcss = function(server_name)
            lspconfig[server_name].setup({
              on_attach = on_attach,
              capabilities = cmp_lsp.default_capabilities(),
              filetypes = {
                "aspnetcorerazor",
                "astro",
                "astro-markdown",
                "blade",
                "django-html",
                "htmldjango",
                "edge",
                "eelixir",
                "elixir",
                "ejs",
                "erb",
                "eruby",
                "gohtml",
                "haml",
                "handlebars",
                "hbs",
                "html",
                "html-eex",
                "heex",
                "jade",
                "leaf",
                "liquid",
                "markdown",
                "mdx",
                "mustache",
                "njk",
                "nunjucks",
                "php",
                "razor",
                "slim",
                "twig",
                "css",
                "less",
                "postcss",
                "sass",
                "scss",
                "stylus",
                "sugarss",
                "javascript",
                "javascriptreact",
                "reason",
                "rescript",
                "typescript",
                "typescriptreact",
                "vue",
                "svelte",
                "rust",
                "rs",
              },
              init_options = {
                userLanguages = {
                  eelixir = "html-eex",
                  eruby = "erb",
                  rust = "html",
                },
              },
              settings = {
                tailwindCSS = {
                  classAttributes = { "class", "className", "classList", "ngClass" },
                  suggestions = true,
                  lint = {
                    cssConflict = "warning",
                    invalidApply = "error",
                    invalidConfigPath = "error",
                    invalidScreen = "error",
                    invalidTailwindDirective = "error",
                    invalidVariant = "error",
                    recommendedVariantOrder = "warning",
                  },
                  validate = true,
                  includeLanguages = {
                    rust = "html",
                  },
                },
              },
            })
          end,
        })
      end
    end,
  },
  {
    "jose-elias-alvarez/null-ls.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    lazy = false,
    config = function()
      local nullls = require("null-ls")
      nullls.setup({
        on_attach = base_on_attach,
        sources = {
          nullls.builtins.diagnostics.eslint_d.with({
            condition = function(utils)
              return utils.root_has_file({
                ".eslintrc.js",
                ".eslintrc.cjs",
                ".eslintrc.yaml",
                ".eslintrc.yml",
                ".eslintrc.json",
              })
            end,
          }),
          nullls.builtins.code_actions.eslint_d.with({
            condition = function(utils)
              return utils.root_has_file({
                ".eslintrc.js",
                ".eslintrc.cjs",
                ".eslintrc.yaml",
                ".eslintrc.yml",
                ".eslintrc.json",
              })
            end,
          }),
          nullls.builtins.formatting.prettier.with({
            prefer_local = "node_modules/.bin",
          }),
          nullls.builtins.formatting.stylua.with({
            condition = function(utils)
              return utils.root_has_file({ "stylua.toml", ".stylua.toml" })
            end,
          }),
          nullls.builtins.formatting.clang_format,
          nullls.builtins.formatting.cmake_format,
          nullls.builtins.formatting.fish_indent,
          nullls.builtins.formatting.fixjson,
          nullls.builtins.diagnostics.fish,
        },
      })

      -- nullls.register({
      --   name = "format_yew_html",
      --   method = nullls.methods.FORMATTING,
      --   filetypes = { "rust" },
      --   generator = {
      --     fn = function(params)
      --       local bufnr = params.bufnr
      --     end,
      --   },
      -- })
    end,
  },
  {
    "RishabhRD/nvim-lsputils",
    dependencies = { "RishabhRD/popfix" },
    lazy = false,
    config = function()
      vim.lsp.handlers["textDocument/codeAction"] = require("lsputil.codeAction").code_action_handler
      vim.lsp.handlers["textDocument/references"] = require("lsputil.locations").references_handler
      vim.lsp.handlers["textDocument/definition"] = require("lsputil.locations").definition_handler
      vim.lsp.handlers["textDocument/declaration"] = require("lsputil.locations").declaration_handler
      vim.lsp.handlers["textDocument/typeDefinition"] = require("lsputil.locations").typeDefinition_handler
      vim.lsp.handlers["textDocument/implementation"] = require("lsputil.locations").implementation_handler
      vim.lsp.handlers["textDocument/documentSymbol"] = require("lsputil.symbols").document_handler
      vim.lsp.handlers["workspace/symbol"] = require("lsputil.symbols").workspace_handler
    end,
  },
  { "jose-elias-alvarez/typescript.nvim" },
  { "nvim-lua/lsp_extensions.nvim" },
  {
    "j-hui/fidget.nvim",
    lazy = false,
    config = function()
      require("fidget").setup({})
    end,
  },
  {
    "ray-x/lsp_signature.nvim",
    lazy = false,
    config = function()
      require("lsp_signature").setup({})
    end,
  },
  {
    "williamboman/mason-lspconfig.nvim",
    config = function()
      require("mason-lspconfig").setup({
        ensure_installed = {
          "bashls",
          "pyright",
          "yamlls",
          "jsonls",
          "sumneko_lua",
          "rust_analyzer",
          "clangd",
          "taplo",
          "tsserver",
          "tailwindcss",
        },
      })
    end,
  },
  {
    "jayp0521/mason-null-ls.nvim",
    config = function()
      require("mason-null-ls").setup({
        ensure_installed = nil,
        automatic_installation = true,
        automatic_setup = true,
      })
    end,
  },
  {
    "smjonas/inc-rename.nvim",
    lazy = false,
    config = function()
      require("inc_rename").setup({
        input_buffer_type = "dressing",
      })
    end,
  },
  {
    "zbirenbaum/copilot-cmp",
    config = function()
      require("copilot_cmp").setup()
    end,
  },
  {
    "hrsh7th/nvim-cmp",
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",
      "L3MON4D3/LuaSnip",
      "saadparwaiz1/cmp_luasnip",
      "onsails/lspkind.nvim",
    },
    event = "InsertEnter",
    config = function()
      local cmp = require("cmp")
      local lspkind = require("lspkind")
      vim.o.completeopt = "menu,menuone,noinsert"
      local only_if_visible = function(callback)
        return function(fallback)
          if cmp.visible() then
            callback(fallback)
          else
            fallback()
          end
        end
      end
      local copilot_cmp = safe_require("copilot_cmp")
      cmp.setup({
        formatting = {
          format = lspkind.cmp_format({
            mode = "symbol_text", -- show only symbol annotations
            maxwidth = 50, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)

            symbol_map = {
              Copilot = "",
            },
          }),
        },
        snippet = {
          expand = function(args)
            require("luasnip").lsp_expand(args.body)
          end,
        },
        mapping = {
          ["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "c" }),
          ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "c" }),
          ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
          ["<C-y>"] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
          ["<C-e>"] = cmp.mapping({
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
          }),
          ["<CR>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
          ["<C-p>"] = cmp.mapping(
            only_if_visible(function()
              cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
            end),
            { "i", "s" }
          ),
          ["<C-k>"] = cmp.mapping(
            only_if_visible(function()
              cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
            end),
            { "i", "s" }
          ),
          ["<C-n>"] = cmp.mapping(
            only_if_visible(function()
              cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
            end),
            { "i", "s" }
          ),
          ["<C-j>"] = cmp.mapping(
            only_if_visible(function()
              cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
            end),
            { "i", "s" }
          ),
          ["<Tab>"] = cmp.mapping(
            only_if_visible(function()
              local entry = cmp.get_selected_entry()
              if not entry then
                cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
              else
                cmp.confirm()
              end
            end),
            { "i", "s" }
          ),
        },
        sources = cmp.config.sources({
          { name = "copilot" },
          { name = "luasnip" },
          { name = "nvim_lsp" },
        }, {
          { name = "buffer" },
        }),
      })

      -- -- Set configuration for specific filetype.
      -- cmp.setup.filetype("gitcommit", {
      --   sources = cmp.config.sources({
      --     { name = "cmp_git" }, -- You can specify the `cmp_git` source if you were installed it.
      --   }, {
      --     { name = "buffer" },
      --   }),
      -- })

      cmp.setup.cmdline("/", {
        sources = {
          { name = "buffer" },
        },
      })

      -- cmp.setup.cmdline(":", {
      --   sources = cmp.config.sources({
      --     { name = "path" },
      --   }, {
      --     { name = "cmdline" },
      --   }),
      -- })
    end,
  },
}
