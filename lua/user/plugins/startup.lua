return {
  "startup-nvim/startup.nvim",
  enabled = false,
  dependencies = { "nvim-lua/plenary.nvim" },
  config = function()
    require("startup").setup({ theme = "dashboard" })
  end,
}
