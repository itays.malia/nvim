return {
  "hkupty/iron.nvim",
  cmd = {
    "IronRepl",
    "IronReplHere",
    "IronRestart",
    "IronSend",
    "IronFocus",
    "IronWatchCurrentFile",
    "IronUnwatchCurrentFile",
  },
  config = function()
    local iron = require("iron")

    iron.core.set_config({
      preferred = {
        python = "ipython",
      },
    })
  end,
}
