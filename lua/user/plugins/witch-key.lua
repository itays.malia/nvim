return {
  "folke/which-key.nvim",
  lazy = false,
  config = function()
    local wk = require("which-key")
    wk.setup({
      ignore_missing = false,
      plugins = {
        presets = {
          operators = false,
          motions = false,
          text_objects = false,
          nav = false,
          g = false,
        },
      },
    })
    wk.register({ ["<leader>x"] = { name = "Harpoon" } })
    wk.register({ ["<leader>s"] = { name = "Telescope" } })
    wk.register({ ["<leader>sg"] = { name = "Git" } })
    wk.register({ ["<leader>t"] = { name = "Terminal" } })
  end,
}
