return {
  "folke/tokyonight.nvim",
  lazy = false,
  priority = 100000,
  config = function()
    vim.o.background = "dark"
    vim.cmd.colorscheme("tokyonight-night")
  end,
}
