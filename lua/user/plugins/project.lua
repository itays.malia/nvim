return {
  "ahmedkhalf/project.nvim",
  lazy = false,
  config = function()
    require("project_nvim").setup({})

    local telescope = safe_require("telescope", true)
    if not telescope then
      return
    end
    telescope.load_extension("projects")

    vim.keymap.set("n", "<leader>sp", telescope.extensions.projects.projects, { desc = "Projects" })
  end,
}
