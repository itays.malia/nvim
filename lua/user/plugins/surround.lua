return {
  "tpope/vim-surround",
  dependencies = { "tpope/vim-repeat" },
  init = function()
    vim.g.surround_no_mappings = true
    vim.keymap.set("v", "S", "s")
  end,
  keys = {
    { "ds", "<Plug>Dsurround" },
    { "cs", "<Plug>Csurround" },
    { "cS", "<Plug>CSurround" },
    { "ys", "<Plug>Ysurround" },
    { "yS", "<Plug>YSurround" },
    { "yss", "<Plug>Yssurround" },
    { "ySs", "<Plug>YSsurround" },
    { "ySS", "<Plug>YSsurround" },
    { "s", "<Plug>VSurround", mode = "v" },
    { "gs", "<Plug>VgSurround", mode = "v" },
  },
}
