return {
  {
    "nvim-orgmode/orgmode",
    enabled = false,
    config = function()
      local orgmode = safe_require("orgmode")
      if not orgmode then
        return
      end

      orgmode.setup({
        org_agenda_files = { "~/org/**/*" },
        org_default_notes_file = "~/org/file.org",
      })

      vim.keymap.set("n", "<leader>o", "<nop>")
      local wk = safe_require("which-key", true)
      if wk then
        wk.register({ ["<leader>o"] = { name = "Org Mode" } })
      end
      vim.keymap.set("n", "<leader>oa", function()
        orgmode.action("agenda.prompt")
      end, { desc = "Agenda Prompt" })
      vim.keymap.set("n", "<leader>oc", function()
        orgmode.action("capture.prompt")
      end, { desc = "Capture Prompt" })
    end,
  },
  {
    "akinsho/org-bullets.nvim",
    enabled = false,
    config = function()
      local org_bullets = safe_require("org-bullets")
      if not org_bullets then
        return
      end
      org_bullets.setup({
        symbols = { "◉", "○", "✸", "✿" },
      })
    end,
  },
}
