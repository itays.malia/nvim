return {
  "ThePrimeagen/harpoon",
  dependencies = { "nvim-lua/plenary.nvim" },
  keys = {
    {
      "<leader>xa",
      function()
        require("harpoon.mark").add_file()
      end,
      desc = "Mark current file",
    },
    {
      "<leader>xd",
      function()
        require("harpoon.mark").rm_file()
      end,
      desc = "Remove mark current file",
    },
    {
      "<leader>xd",
      function()
        require("harpoon.mark").toggle_file()
      end,
      desc = "Toggle mark current file",
    },
    {
      "<leader>xc",
      function()
        require("harpoon.mark").clear_all()
      end,
      desc = "Clear all marks",
    },
    {
      "<leader>sc",
      function()
        require("telescope").extensions.harpoon.marks()
      end,
      desc = "Harpoon Marks",
    },
    {
      "<leader>xs",
      function()
        require("telescope").extensions.harpoon.marks()
      end,
      desc = "Harpoon Marks",
    },
  },
  config = function()
    local harpoon = require("harpoon")
    harpoon.setup({})
    local telescope = safe_require("telescope", true)
    if telescope then
      telescope.load_extension("harpoon")
    end
  end,
}
