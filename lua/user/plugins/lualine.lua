return {
  "nvim-lualine/lualine.nvim",
  requires = { "kyazdani42/nvim-web-devicons" },
  lazy = false,
  config = function()
    local lualine = require("lualine")
    local section_c = { { "filename", path = 1 } }

    local section_x = {
      "o:keymap",
      "encoding",
      "fileformat",
      "filetype",
    }

    local gps = safe_require("nvim-gps", true)
    if gps then
      table.insert(section_c, { gps.get_location, cond = gps.is_available })
    end

    lualine.setup({
      sections = {
        lualine_c = section_c,
        lualine_x = section_x,
      },
    })
  end,
}
