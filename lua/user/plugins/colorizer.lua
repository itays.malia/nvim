return {
  "norcalli/nvim-colorizer.lua",
  ft = { "css", "javascript", "html" },
  config = function()
    require("colorizer").setup({ "css", "javascript", "html" })
  end,
}
