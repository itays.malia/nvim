return {
  "rcarriga/nvim-notify",
  config = function()
    local notify = require("notify")
    notify.setup({
      timeout = 1000,
      max_width = 60,
      max_hight = 2,
    })

    vim.notify = notify
    local telescope = safe_require("telescope", true)
    if telescope then
      vim.api.nvim_create_user_command("Messages", telescope.extensions.notify.notify, { nargs = 0 })
    end

    vim.api.nvim_create_autocmd("FileType", {
      pattern = "notify",
      callback = function()
        vim.keymap.set("n", "q", vim.cmd.q, { buffer = 0 })
      end,
    })
  end,
}
