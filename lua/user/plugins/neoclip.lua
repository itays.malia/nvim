return {
  "AckslD/nvim-neoclip.lua",
  dependencies = { "tami5/sqlite.lua" },
  config = function()
    require("neoclip").setup({
      history = 1000,
      enable_persistent_history = true,
      preview = true,
      default_register = '"',
      default_register_macros = "q",
      enable_macro_history = true,
      keys = {
        telescope = {
          i = {
            select = "<cr>",
            paste = "<c-p>",
            paste_behind = "<c-o>",
            replay = "<c-q>",
            custom = {},
          },
          n = {
            select = "<cr>",
            paste = "p",
            paste_behind = "P",
            replay = "q",
            custom = {},
          },
        },
      },
    })
    local telescope = safe_require("telescope", true)
    if telescope then
      telescope.load_extension("neoclip")
      vim.keymap.set("n", "<leader>sc", telescope.extensions.neoclip.neoclip, { desc = "Clipboard History" })
    end
  end,
}
