return {
  "sunjon/shade.nvim",
  enabled = false,
  config = function()
    require("shade").setup({})
  end,
}
