return {
  "zbirenbaum/copilot.lua",
  event = "VimEnter",
  config = function()
    vim.defer_fn(function()
      require("copilot").setup()
    end, 100)

    -- vim.g.copilot_no_tab_map = true
    -- vim.keymap.set("i", "<C-J>", function()
    --   vim.cmd([[copilot#Accept("\<CR>")]])
    -- end)
  end,
}
