return {
  "stevearc/vim-arduino",
  ft = { "arduino" },
  config = function()
    vim.api.nvim_create_autocmd("FileType", {
      pattern = "arduino",
      callback = function()
        local wk = safe_require("which-key", true)
        if wk then
          wk.register({ ["<leader>a"] = { name = "Arduino", buffer = 0 } })
        end

        vim.keymap.set("n", "<leader>aa", vim.cmd.ArduinoAttach, { desc = "Attach", buffer = 0 })
        vim.keymap.set("n", "<leader>am", vim.cmd.ArduinoVerify, { desc = "Verify", buffer = 0 })
        vim.keymap.set("n", "<leader>au", vim.cmd.ArduinoUpload, { desc = "Upload", buffer = 0 })
        vim.keymap.set(
          "n",
          "<leader>ad",
          vim.cmd.ArduinoUploadAndSerial,
          { desc = "Upload and open serial", buffer = 0 }
        )
        vim.keymap.set("n", "<leader>as", vim.cmd.ArduinoSerial, { desc = "Open Serial", buffer = 0 })
        vim.keymap.set("n", "<leader>ab", vim.cmd.ArduinoChooseBoard, { desc = "Choose Board", buffer = 0 })
        vim.keymap.set("n", "<leader>ap", vim.cmd.ArduinoChooseProgrammer, { desc = "Choose Programmer", buffer = 0 })
      end,
    })
  end,
}
