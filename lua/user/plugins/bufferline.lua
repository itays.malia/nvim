local icons = {
  error = " ",
  warning = " ",
  info = "",
}

return {
  "akinsho/bufferline.nvim",
  requires = "kyazdani42/nvim-web-devicons",
  lazy = false,
  version = "v2.*",
  keys = {
    {
      "<A-1>",
      function()
        require("bufferline").go_to(1, true)
      end,
    },
    {
      "<A-2>",
      function()
        require("bufferline").go_to(2, true)
      end,
    },
    {
      "<A-3>",
      function()
        require("bufferline").go_to(3, true)
      end,
    },
    {
      "<A-4>",
      function()
        require("bufferline").go_to(4, true)
      end,
    },
    {
      "<A-5>",
      function()
        require("bufferline").go_to(5, true)
      end,
    },
    {
      "<A-6>",
      function()
        require("bufferline").go_to(6, true)
      end,
    },
    {
      "<A-7>",
      function()
        require("bufferline").go_to(7, true)
      end,
    },
    {
      "<A-8>",
      function()
        require("bufferline").go_to(8, true)
      end,
    },
    {
      "<A-9>",
      function()
        require("bufferline").go_to(9, true)
      end,
    },
    {
      "<leader>1",
      function()
        require("bufferline").go_to(1, true)
      end,
      desc = "Goto Buf 1",
    },
    {
      "<leader>2",
      function()
        require("bufferline").go_to(2, true)
      end,
      desc = "Goto Buf 2",
    },
    {
      "<leader>3",
      function()
        require("bufferline").go_to(3, true)
      end,
      desc = "Goto Buf 3",
    },
    {
      "<leader>4",
      function()
        require("bufferline").go_to(4, true)
      end,
      desc = "Goto Buf 4",
    },
    {
      "<leader>5",
      function()
        require("bufferline").go_to(5, true)
      end,
      desc = "Goto Buf 5",
    },
    {
      "<leader>6",
      function()
        require("bufferline").go_to(6, true)
      end,
      desc = "Goto Buf 6",
    },
    {
      "<leader>7",
      function()
        require("bufferline").go_to(7, true)
      end,
      desc = "Goto Buf 7",
    },
    {
      "<leader>8",
      function()
        require("bufferline").go_to(8, true)
      end,
      desc = "Goto Buf 8",
    },
    {
      "<leader>9",
      function()
        require("bufferline").go_to(9, true)
      end,
      desc = "Goto Buf 9",
    },
    {
      "gb",
      function()
        require("bufferline").cycle(1)
      end,
    },
    {
      "gB",
      function()
        require("bufferline").cycle(-1)
      end,
    },
  },
  config = function()
    local bufferline = require("bufferline")
    vim.opt.termguicolors = true
    bufferline.setup({
      options = {
        -- diagnostics = "nvim_lsp",
        -- offsets = {
        --   {
        --     filetype = "NvimTree",
        --     text = "File Explorer",
        --     highlight = "Directory",
        --     text_align = "left",
        --   },
        -- },
        -- custom_areas = {
        --   right = function()
        --     local result = {}
        --     local seve = vim.diagnostic.severity
        --     local error = #vim.diagnostic.get(0, { severity = seve.ERROR })
        --     local warning = #vim.diagnostic.get(0, { severity = seve.WARN })
        --     local info = #vim.diagnostic.get(0, { severity = seve.INFO })
        --     local hint = #vim.diagnostic.get(0, { severity = seve.HINT })

        --     table.insert(result, { text = " iwork " .. error, guifg = "#EC5241" })
        --     if error ~= 0 then
        --       table.insert(result, { text = "  " .. error, guifg = "#EC5241" })
        --     end

        --     if warning ~= 0 then
        --       table.insert(result, { text = "  " .. warning, guifg = "#EFB839" })
        --     end

        --     if hint ~= 0 then
        --       table.insert(result, { text = "  " .. hint, guifg = "#A3BA5E" })
        --     end

        --     if info ~= 0 then
        --       table.insert(result, { text = "  " .. info, guifg = "#7EA9A7" })
        --     end
        --     return result
        --   end,
        -- },
        diagnostics_indicator = function(count, level, diagnostics_dict, context)
          local s = " "
          for e, n in pairs(diagnostics_dict) do
            local sym = icons[e]
            s = s .. n .. sym
          end
          return s
        end,
      },
    })
  end,
}
