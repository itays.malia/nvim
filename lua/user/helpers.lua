vim.api.nvim_create_user_command("SudoWrite", function(t)
  local args = t.args
  if args == "" then
    vim.cmd('w !SUDO_ASKPASS="' .. vim.env.HOME .. '/.config/nvim/askpass.sh" sudo -A tee % > /dev/null')
  else
    vim.cmd('w !SUDO_ASKPASS="' .. vim.env.HOME .. '/.config/nvim/askpass.sh"sudo tee ' .. args .. " > /dev/null")
  end
end, { nargs = "*", bang = false, complete = "file" })

vim.api.nvim_create_user_command("Mkdir", function(t)
  if t.args == "" then
    vim.cmd("!mkdir -p %")
  else
    vim.cmd('!mkdir -p "' .. t.args .. '"')
  end
end, { nargs = "*", bang = false, complete = "dir" })
