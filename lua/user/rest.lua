local packer = require("user.packer")
if packer == nil then
  return
end
local use = packer.use

use({
  "NTBBloodbath/rest.nvim",
  requires = { "nvim-lua/plenary.nvim" },
  config = function()
    local rest_nvim = safe_require("rest-nvim")
    if not rest_nvim then
      return
    end
    rest_nvim.setup({
      -- Open request results in a horizontal split
      result_split_horizontal = false,
      -- Keep the http file buffer above|left when split horizontal|vertical
      result_split_in_place = false,
      -- Skip SSL verification, useful for unknown certificates
      skip_ssl_verification = false,
      -- Highlight request on run
      highlight = {
        enabled = true,
        timeout = 150,
      },
      result = {
        -- toggle showing URL, HTTP info, headers at top the of result window
        show_url = true,
        show_http_info = true,
        show_headers = true,
      },
      -- Jump to request line on run
      jump_to_request = false,
      env_file = ".env",
      custom_dynamic_variables = {},
      yank_dry_run = true,
    })

    local wk = safe_require("which-key", true)
    if wk then
      wk.register({ ["<leader>r"] = { name = "Rest" } })
    end
    vim.keymap.set("n", "<leader>rr", "<Plug>RestNvim", { desc = "Run" })
    vim.keymap.set("n", "<leader>rp", "<Plug>RestNvimPreview", { desc = "Preview" })
    vim.keymap.set("n", "<leader>rR", "<Plug>RestNvimLast", { desc = "Re-Run" })

    vim.api.nvim_create_autocmd("FileType", {
      pattern = "httpResult",
      callback = function()
        vim.keymap.set("n", "q", vim.cmd.q, { buffer = 0 })
      end,
    })
  end,
})
