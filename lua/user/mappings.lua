vim.keymap.set("n", "<leader>p", '"+p', { desc = "Clipbord Paste" })
vim.keymap.set("v", "<leader>p", '"+p', { desc = "Clipbord Paste" })
vim.keymap.set("n", "<leader>P", '"+P', { desc = "Clipbord Paste" })
vim.keymap.set("n", "<leader>y", '"+y', { desc = "Clipbord Yank" })
vim.keymap.set("v", "<leader>y", '"+y', { desc = "Clipbord Yank" })

vim.keymap.set("n", "<leader>l", require("user.hebrew").toggle_hebrew, { desc = "Toggle Hebrew" })
vim.keymap.set("n", "<leader>w", vim.cmd.w, { desc = "Save" })

vim.keymap.set("t", "<C-j><C-j>", "<C-\\><C-N>", { silent = true })

