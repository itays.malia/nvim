local M = {}

function M.toggle_hebrew()
  if vim.opt.keymap:get() ~= "" then
    vim.opt.keymap = ""
  else
    vim.opt.keymap = "hebrew"
  end
end

return M
