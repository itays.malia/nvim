require("user.utils")
--
-- use({ "alec-gibson/nvim-tetris", cmd = "Tetris" })
-- -- use { 'chaoren/vim-wordmotion' }
-- use({ "romainl/vim-cool" })
-- -- use({ "tpope/vim-eunuch" })
-- -- use({ "yamatsum/nvim-cursorline" })
-- use("lewis6991/impatient.nvim")
-- use("kamailio/vim-kamailio-syntax")

require("user.base")
require("user.helpers")

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup("user.plugins", {
  defaults = {
    lazy = true,
  },
})
